package validator;


import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import src.java.Utils.ConstantsUtil;
/**
 *
 * @author ishani
 */
@ManagedBean(name="checkSelectedValidator")
@SessionScoped
public class CheckSelectedValidator{
     
    public void valueExists(FacesContext context, UIComponent comp, Object value) throws ValidatorException {
        String checkYesOrNo=(String)value;
       if(checkYesOrNo==null)
    {
       
FacesContext.getCurrentInstance().getPartialViewContext();

   FacesMessage message = new FacesMessage(
					"Please Select one value");
			context.addMessage(comp.getClientId(context), message); 
    
    throw new ValidatorException(message);
    }
    }
    
}
