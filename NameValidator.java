package validator;


import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author ishani
 */
@ManagedBean(name="nameValidate")
@SessionScoped
public class NameValidator{

   
    public void nameValidate(FacesContext context, UIComponent comp, Object value) throws ValidatorException {
        String name=(String)value;
       if(name.equals(" ") || name.length()>2 )
    {
       
FacesContext.getCurrentInstance().getPartialViewContext();

   FacesMessage message = new FacesMessage(
					"Please Enter your Name");
			context.addMessage(comp.getClientId(context), message); 
    
    throw new ValidatorException(message);
    }
    }
    
}
