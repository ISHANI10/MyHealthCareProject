

package src.java.Service;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import src.java.Dao.HibernateDao;
import src.java.Entity.Client;
import src.java.Entity.Habit;
import src.java.Entity.Health;
import src.java.Utils.ConstantsUtil;

/**
 *
 * @author ishani
 */
public class BusinessTest {
  Business objBusiness=new Business();
  Client objClient=new Client();
  Habit objHabit=new Habit();
  Health objHealth=new Health();
  double amount=ConstantsUtil.BASE_PREMIUM;

  
// Result Check
    @Test
    public void testServices() {
        System.out.println("services Test");
        objClient.setName("Norman Gomes");
        objClient.gender="male";
        objClient.age=34;
        objHealth.setBlood_Pressure("no");
        objHealth.setBlood_Sugar("no");
        objHealth.setHypertension("no");
        objHealth.setOverweight("yes");
        objHabit.setAlcohol("yes");
        objHabit.setDaily_Exercise("Yes");
        objHabit.setDrugs("no");
        objHabit.setSmoking("no");
        amount=objBusiness.genderBased(amount,objClient.gender);
        amount=objBusiness.calculateBasePremium(amount,objClient.age);
        amount=objBusiness.pre_Existing_Health_Conditions(objHealth, amount);
        amount=objBusiness.pre_Existing_Habit_Conditions(objHabit, amount);
        double expResult = 6856;
        assertEquals(expResult, Math.round(amount),0.0);

    }
  @Test
    public void testCalculatePercentage() {
        System.out.println("calculatePercentage Test");
        int percentage = 10;
        double expResult = 0.0;
        double result = objBusiness.calculatePercentage(amount, percentage);
        assertEquals(5500, result, 0.0);
  }
   
    @Test
    public void testCalculateBasePremium() {
        System.out.println("calculateBasePremium Test");
        int age = 17;
        double expResult = 5000.0;
        double result = objBusiness.calculateBasePremium(amount, age);
        assertEquals(expResult, result, 0.0);
        
    }

  
    @Test
    public void testGenderBased() {
        System.out.println("genderBased Test");
        String gender = "male";
        double expResult = 5100.0;
        double result = objBusiness.genderBased(amount, gender);
        assertEquals(expResult, result, 0.0);
       
    }

   
    @Test
    public void testPre_Existing_Health_Conditions() {
    System.out.println("pre_Existing_Health_Conditions Test Results Test");
    objHealth.setBlood_Pressure("no");
    objHealth.setBlood_Sugar("no");
    objHealth.setHypertension("no");
    objHealth.setOverweight("no");
    double expResult = 5000.0;
    double result = objBusiness.pre_Existing_Health_Conditions(objHealth, amount);
    assertEquals(expResult, result, 0.0);
  
    }


    @Test
    public void testPre_Existing_Habit_Conditions() {
        System.out.println("pre_Existing_Habit_Conditions Test");
        double expResult = 5150.0;
        objHabit.setAlcohol("yes");
        objHabit.setDaily_Exercise("Yes");
        objHabit.setDrugs("no");
        objHabit.setSmoking("yes");
        double result = objBusiness.pre_Existing_Habit_Conditions(objHabit, amount);
        assertEquals(expResult, result, 0.0);
       
    }

    @Test
    public void testIncrement() {
        System.out.println("incrementStep Test ");
        int step = 2;
        int value = 6;
        int expResult = 8;
        int result = objBusiness.increment(step, value);
        assertEquals(expResult, result);
    }

   @Test
    public void testCheckHealthConditions() {
        System.out.println("checkHealthConditions test");
        objHealth.setBlood_Pressure("no");
    objHealth.setBlood_Sugar("no");
    objHealth.setHypertension("no");
    objHealth.setOverweight("no");
    double expResult = 5000.0;
    double result = objBusiness.pre_Existing_Health_Conditions(objHealth, amount);
    assertEquals(expResult, result, 0.0);
    }

   @Test
    public void testCheckHabits() {
        System.out.println("checkHabits Test");
        double expResult = 5150.0;
        objHabit.setAlcohol("yes");
        objHabit.setDaily_Exercise("yes");
        objHabit.setDrugs("no");
        objHabit.setSmoking("yes");
        double result = objBusiness.pre_Existing_Habit_Conditions(objHabit, amount);
        assertEquals(expResult, result, 0.0);
    }


    
}