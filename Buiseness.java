package service;


import javax.faces.bean.ManagedBean;

import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import src.java.Dao.HibernateDao;
import src.java.Controller.Runner;
import src.java.Entity.Client;
import src.java.Entity.Habit;
import src.java.Entity.Health;
import src.java.Utils.ConstantsUtil;

/**
 *
 * @author ishani
 */
@ManagedBean(name="business")
@SessionScoped
public class Business {
    
@ManagedProperty("#{client}")
public Client client;
@ManagedProperty("#{health}")
public Health health;
@ManagedProperty("#{habits}")
public Habit  habits;
@ManagedProperty("#{hibernateDao}")
public HibernateDao hibernateDao;
private double finalAmount;
/**
     * @param args the command line arguments
     */
    public double calculatePercentage(double amount,int percentage){

    return (amount+((percentage*amount)/100));
}
public void services()
{
  double amount=ConstantsUtil.BASE_PREMIUM;// BaseAmount
   
   amount=genderBased(amount,client.gender);
   amount=calculateBasePremium(amount,client.age);
   amount=pre_Existing_Health_Conditions(health, amount);
   amount=pre_Existing_Habit_Conditions(habits, amount);
   client.finalAmount=Math.ceil(amount);
}
 public double calculateBasePremium(double amount,int age)
    {
        int clientAge=age;
        int a=25;// start Pointer
        int b=30; // End pointer
        int percentageInc=ConstantsUtil.BELOW_40_PERCENTAGE; // percentage increase for below 40 age
        double calculatedAmount=amount;
         if(clientAge<18)
        {
        return amount;
        }
         else if(clientAge>=18 && clientAge<25)
         {
         return calculatePercentage(calculatedAmount, percentageInc);
         }
       calculatedAmount=calculatePercentage(calculatedAmount, percentageInc);
       
       while(a<clientAge){
           
       if(a>=40){
       percentageInc=ConstantsUtil.ABOVE_40_PERCENTAGE;
       
       }
       
       calculatedAmount=calculatePercentage(calculatedAmount, percentageInc);
       a=increment(a,5);
       b=increment(b,5);
       }
       return calculatedAmount;
    }
public double genderBased(double amount,String gender)
{
    
if(gender.equalsIgnoreCase(ConstantsUtil.MALE)){
return calculatePercentage(amount,ConstantsUtil.GENDER_MALE_PERCENTAGE);
}
return amount;
}

   public double pre_Existing_Health_Conditions(Health h,double amount){
       Health conditions=h;
       int percentage=checkHealthConditions(conditions,amount);
       return calculatePercentage(amount, percentage);
   }
   public double pre_Existing_Habit_Conditions(Habit h,double amount){
       Habit conditions=h;
       int percentage=checkHabits(conditions,amount);
       return calculatePercentage(amount, percentage);
   }
   
     synchronized int increment(int step,int value){
         return value+step;
       }

    public int checkHealthConditions(Health conditions, double amount) {
        int percentage=0;
      if(conditions.getBlood_Pressure().equalsIgnoreCase(ConstantsUtil.YES)){
        percentage+=ConstantsUtil.HEALTH_CONDITION_PERCENTAGE;
        }
        if(conditions.getBlood_Sugar().equalsIgnoreCase(ConstantsUtil.YES)){
        percentage+=ConstantsUtil.HEALTH_CONDITION_PERCENTAGE;
        }
        if(conditions.getHypertension().equalsIgnoreCase(ConstantsUtil.YES)){
        percentage+=ConstantsUtil.HEALTH_CONDITION_PERCENTAGE;
        }
        if(conditions.getOverweight().equalsIgnoreCase(ConstantsUtil.YES)){
        percentage+=ConstantsUtil.HEALTH_CONDITION_PERCENTAGE;
        }
    return percentage ;
    }

    public int checkHabits(Habit conditions, double amount) {
         int percentage=0;
        if(conditions.getAlcohol().equalsIgnoreCase(ConstantsUtil.YES)){
        percentage+=ConstantsUtil.HABIT_BAD_PERCENTAGE;
        }
        if(conditions.getDaily_Exercise().equalsIgnoreCase(ConstantsUtil.YES)){
        percentage+=ConstantsUtil.HABIT_GOOD_PERCENTAGE;
        }
        if(conditions.getDrugs().equalsIgnoreCase(ConstantsUtil.YES)){
        percentage+=ConstantsUtil.HABIT_BAD_PERCENTAGE;
        }
        if(conditions.getSmoking().equalsIgnoreCase(ConstantsUtil.YES)){
        percentage+=ConstantsUtil.HABIT_BAD_PERCENTAGE;
        }
        return percentage;
    }
     public HibernateDao getHibernateDao() {
        return hibernateDao;
    }

    public void setHibernateDao(HibernateDao hibernateDao) {
        this.hibernateDao = hibernateDao;
    }




    public Habit getHabits() {
        return habits;
    }

    public void setHabits(Habit habits) {
        this.habits = habits;
    }

    public double getFinalAmount() {
        return finalAmount;
    }
    
    public void setFinalAmount(double finalAmount) {
        this.finalAmount = finalAmount;
    }
   
    public Business() {
       
    }
    
private static int base_Premium=5000;

 public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }


 

    public Health getHealth() {
        return health;
    }

    public void setHealth(Health health) {
        this.health = health;
    }
}