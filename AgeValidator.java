package validator;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author ishani
 */

@ManagedBean(name="validator")
@SessionScoped
public class AgeValidator {
     
    public void ageValidator(FacesContext context, UIComponent comp, Object value) throws ValidatorException {
        int age=(int)value;
       if(age<=0 || age>130)
    {
       
FacesContext.getCurrentInstance().getPartialViewContext();

   FacesMessage message = new FacesMessage(
					"Please Enter valid Age (0-130)");
			context.addMessage(comp.getClientId(context), message); 
    
    throw new ValidatorException(message);
    }
  
       
    }
    
}